import React from 'react';
import {readShop} from "../services/ApiServices/shopAPI";
import {getProductShop} from "../services/ApiServices/productAPI";
import 'antd/dist/antd.css';
import {Tabs, Table, Tag, Space} from 'antd';
import "../assets/DetailShop.css"
import {useEffect, useState} from "react";
import {NavLink, useParams} from "react-router-dom";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";

const {TabPane} = Tabs;

const DetailShop = () => {
  const {id} = useParams();
  console.log(id)
  const [shop, setShop] = useState({});
  const [products, setProducts] = useState([]);

  useEffect(() => {
    getShop();
    getProductShops()
  }, [id]);
  const getShop = async () => {
    try {

      const {data} = await readShop(id);

      setShop(data);
    } catch (error) {
    }
  };
  const getProductShops = async () => {
    try {
      const {data} = await getProductShop(id);
      setProducts(data.results);
    } catch (error) {
    }
  };


  const columns = [
    {
      title: 'Tên sản phẩm',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Giá tiền',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Image',
      dataIndex: 'img',
      key: 'img',
      render: text => <img src={text} width="100px"/>,
    },
    {
      title: 'Số lượng',
      dataIndex: 'quantity',
      key: 'quantity',
    },
    {
      title: 'Mô tả',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">

          <DeleteOutlined/>
        </Space>
      ),
    },
  ]
console.log(products)
  return (
    <div>
      <h2>Chi tiết cửa hàng</h2>
      <Tabs defaultActiveKey="1">
        <TabPane tab="Thông tin cửa hàng" key="1">
          <div className="detail-shop-container">
            <div className="info-shop-box">
              <h2>Tên cửa hàng: {shop.name}</h2>
              <img src={shop.avatar} width={150}/>
              <h2>Địa chỉ cửa hàng: {shop.adress}</h2>
              <h2>Gói bán hàng đang áp dụng: {shop.package}đ/tháng</h2>
              {/*{shop.categories.length > 0 && (*/}
              {/*  <h2>Số danh mục đang sử dụng: {shop.categories.length}</h2>*/}
              {/*)}*/}
            </div>
          </div>
        </TabPane>
        <TabPane tab="Sản phẩm của cửa hàng" key="2">
          <div className="detail-product-shop-container">
            <div className="info-shop-box">
              <Table columns={columns} dataSource={products}/>
            </div>
          </div>
        </TabPane>
      </Tabs>
    </div>
  )
}
export default DetailShop