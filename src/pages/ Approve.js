import React from 'react';
import {Button, Input, Select, Table, Modal, Pagination} from "antd";
import {getProductAction, updateAllProduct, updateProduct} from "../services/ApiServices/productAPI";
import {useEffect, useState} from "react";
import {ToastContainer, toast} from "react-toastify";
import {ExclamationCircleOutlined} from "@ant-design/icons";

const {confirm} = Modal;
const {Search} = Input;
const {Option} = Select;
const ApproveProduct = () => {
  const [products, setProducts] = useState([]);
  const [selectionType, setSelectionType] = useState('checkbox');
  const [loading,setLoading] = useState(false)
  const [page,setPage] = useState(1);
  const [totalPage,setTotalPage] = useState(0)
  let limit =12
  const columns = [
    {
      title: 'Tên SP',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Hình ảnh',
      render: (record => {
        return (
          <img width="50px" src={record.img}/>
        )
      }),
      key: 'img',
    },
    {
      title: 'Cửa hàng',
      render: (record => {
        return (
          <div>{record.shop.name}</div>
        )
      }),
      key: 'shop',
    },
    {
      title: 'Danh mục',
      render: (record => {
        return (
          <div>{record.categories?.name}</div>
        )
      })
    },
    {
      title: 'Giá',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Trạng thái',
      render: (record => {
        return (
          <Select defaultValue={record.action}
                  onChange={(e) => handleChange(e, record)}>
            <Option value="pending">Chờ xác nhận</Option>
            <Option value="active">Xác nhận</Option>
            <Option value="refuse">Từ chối</Option>
          </Select>
        )
      }),
      key: 'status',
    },

  ];

  const handleChange = async (value, product) => {
    console.log(`selected ${value}`);
    await updateProduct(product.id, {
      action: value
    }).then(res => {
      console.log(res)
      if (res.status === 200) {
        toast.success("Cập nhật trạng thái thành công");
      }
    }).catch(err => {
      console.log(err)
    })
  }

  useEffect(() => {
    getActionProduct()
  }, [])

  function showConfirm() {
    confirm({
      title: 'Do you Want to delete these items?',
      icon: <ExclamationCircleOutlined/>,
      content: 'Some descriptions',
      onOk: async () => {
        await updateAllProduct().then((res)=>{
          if(res.status === 200) {
            toast.success("Đã phê duyệt tất cả");
          }
        })
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  const getActionProduct = async () => {
    setLoading(true)
    const {data} = await getProductAction("pending",page,limit);
    console.log(data)
    setTotalPage(data.totalPages);
    setProducts(data.results);
    setLoading(false)
  }

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(selectedRows)
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name,
    }),
  };
  const handlePagination = async (value) =>{
    setLoading(true)
    const {data} = await getProductAction("pending",value,limit);
    setProducts(data.results)
    setLoading(false)
  }
  return (
    <div>
      <ToastContainer/>
      <h1>Phê duyệt sản phẩm</h1>
      <div style={{display: "flex", justifyContent: "space-between", marginBottom: "20px"}}>
        <Search placeholder="input search text" style={{width: 300}}/>
        <Button onClick={showConfirm}>Phê duyệt tất cả</Button>
      </div>
      <div>
        <Table
          loading={loading}
          pagination={false}
          rowSelection={{
            type: selectionType,
            ...rowSelection,
          }}
          columns={columns}
          dataSource={products}
        />
        <Pagination style={{marginTop:"20px"}} onChange={handlePagination} defaultCurrent={1} total={parseInt(`${totalPage}0`)} />
      </div>
    </div>
  )
}
export default ApproveProduct