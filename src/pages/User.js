import {Table, Tag, Space, Input, Select} from 'antd';
import React, {useEffect, useState} from 'react';
// material
import {getUsers, getUsersSortBy} from "../services/ApiServices/authApi";
import {EyeOutlined, DeleteOutlined} from '@ant-design/icons'
import {getShopSortBy} from "../services/ApiServices/shopAPI";

export default function User() {
  const [users, setUsers] = useState([])
  const [loading, setLoading] = useState(true)
  const [page, setPage] = useState(1)
  const [pageSize, setPageSize] = useState(5)
  const [dataSearch, setDataSearch] = useState(null);

  const { Option } = Select;
  useEffect(() => {
    getDataUsers();
  }, []);

  const getDataUsers = async () => {
    const {data} = await getUsers();
    setUsers(data.results);
    setLoading(false)
  }

  const handleChangeOption = async (value) => {
    const { data } = await getUsersSortBy(1,24,value);
    setUsers(data.results);
  }

  const columns = [
    {
      title: 'Username',
      dataIndex: 'username',
      key: 'username',
      render: text => <a>{text}</a>,
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'CCCD',
      dataIndex: 'cardId',
      key: 'cardId',
    },
    {
      title: 'Role',
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'address',
      key: 'address',
    },
    {
      // title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">
          <a><EyeOutlined/></a>
          <a><DeleteOutlined/></a>
        </Space>
      ),
    },
  ];

  const handleSearch = (value) => {
    const filterTable = users.filter(item =>
      Object.keys(item).some(k =>
        String(item[k])
          .toLowerCase()
          .includes(value.toLowerCase())
      )
    );
    setDataSearch(filterTable)
  }


  return (
    <div>
      <h1>Quản lý người dùng</h1>
      <div style={{ display: "flex", flexDirection: "row-reverse"}}>
        <div>
          <Input.Search
            placeholder="Search by..."
            enterButton
            onSearch={handleSearch}
          />
        </div>
        <div style={{width: '100%'}}>
          <span className="mt-1">Sắp xếp:</span>
          <Select
            defaultValue="Sắp xếp theo"
            style={{marginLeft:"10px",width: '150px'}}
            onChange={handleChangeOption}
          >
            <Option  value="username" style={{width: '100%'}}>Từ A đến Z</Option>
            <Option value="-username" style={{width: '100%'}}>Từ Z đến A</Option>

          </Select>
        </div>
      </div>

      <Table loading={loading}
             columns={columns}
             dataSource={dataSearch == null ? users : dataSearch}
             style={{margin: "20px 0"}}
             pagination={{
               current: page,
               pageSize: pageSize,
               onChange: (page, pageSize) => {
                 setPage(page);
                 setPageSize(pageSize)
               }
             }}
      />
    </div>
  );
}
