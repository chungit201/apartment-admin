import React, {useState, useEffect} from 'react';
import {TextField, Button, Input, Container, IconButton, Select, MenuItem, InputLabel} from '@mui/material';
import {useForm} from 'react-hook-form'
import {addCategory} from '../services/ApiServices/categoryApi'
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import "react-toastify/dist/ReactToastify.css";
import {ToastContainer, toast} from "react-toastify";
import {storage} from "../firebase";

const AddCategoryPage = () => {
    const [categories, setCategories] = useState([])

    const {
        register,
        handleSubmit,
    } = useForm();

    const onSubmit = async (category) => {
        try {
            // call api
            console.log("okokokok")
            const image = category.img[0];
            console.log(image)
            const ref = storage.ref(`images/${image.name}`);
            const upload = ref.put(image);
            upload.on(
                "state_changed",
                snapshot => {
                },
                error => {
                    console.log(error);
                }, () => {
                    storage.ref('images')
                        .child(image.name)
                        .getDownloadURL()
                        .then(async (url) => {
                            const {data} = await addCategory(
                                {
                                    ...category,
                                    img: url
                                }
                            )
                        })
                }
            )
            toast.success("Thêm danh mục thành công");
        } catch (error) {

        }
    };
    console.log(categories);

    return <Container>
        <ToastContainer/>
        <form onSubmit={handleSubmit(onSubmit)}>
            <div style={{marginBottom: "20px"}}>
                <label>
                    <h1>Thêm danh mục mới</h1>
                </label>
            </div>
            <div style={{marginBottom: "20px"}}>
                <TextField
                    sx={{width: "100%"}}
                    {...register('name')}
                    required
                    id="outlined-required"
                    label="Tên danh mục"
                />
            </div>

            <div>
                <label htmlFor="icon-button-file">
                    <Input accept="image/*" id="icon-button-file" type="file" {...register('img')}/>
                    <IconButton color="primary" aria-label="upload picture" component="span">
                        <PhotoCamera/>
                    </IconButton>
                </label>
            </div>

            <div style={{marginBottom: "20px"}}>

                <Button type="submit" variant="contained">Thêm</Button>

            </div>
        </form>
    </Container>
};

export default AddCategoryPage;
