import {Table, Space, notification, Switch, Input, Select} from 'antd';
import React, {useEffect, useState} from 'react';
import {NavLink} from 'react-router-dom'
import {EyeTwoTone} from '@ant-design/icons'
import {getShop, updateShop, getShopSortBy} from "../services/ApiServices/shopAPI";
import "react-toastify/dist/ReactToastify.css";
import {ToastContainer, toast} from "react-toastify";
import {lockProduct} from "../services/ApiServices/productAPI";

const ShopPage = () => {
  const [shops, setShops] = useState([])
  const [loading, setLoading] = useState(true)
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [dataSearch, setDataSearch] = useState(null);
  const [checkedShop, setCheckedShop] = useState(true)
  const {Option} = Select;
  useEffect(() => {
    getDataShop();
  }, []);

  const getDataShop = async () => {
    const {data} = await getShop();
    setShops(data.results);
    setLoading(false)
  }

  const onChangeSwitch = async (checked, id) => {
    setCheckedShop(checked)
    await lockProduct({
      shop: id,
      action: checked ? "active" : "lock"
    })
    try {
      await updateShop(id, {
        lock: !checked
      }).then(response => {
        if (response.status === 200) {
          checked == true ? toast.success("Đã mở khóa shop shop") : toast.success("Đã vô hiệu hóa shop");
        }
      })
    } catch (error) {

    }

  }

  const handleChangeOption = async (value) => {
    const {data} = await getShopSortBy(1, 24, value);
    setShops(data.results);
  }
  const columns = [
    {
      title: 'Tên cửa hàng',
      key: 'name',
      render: (record) => (

        <div>{record.name}</div>
      )
    },
    {
      title: 'Username',
      key: 'author',
      dataIndex: 'author',
      render: (record) => (
        <div>{record.username}</div>
      )
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'author',
      key: 'author',
      render: (record) => (
        <div>{record.phone}</div>

      )
    },
    {
      title: 'Địa chỉ',
      key: 'adress',
      render: (record) => (
        <div>{record.adress}</div>
      )
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">

          <NavLink to={`${record.id}`}><EyeTwoTone style={{fontSize: "20px"}}/></NavLink>
          <Switch defaultChecked={!record.lock} onChange={(e) => onChangeSwitch(e, record.id)}/>


        </Space>
      ),
    },
  ];
  const handleSearch = (value) => {
    const filterTable = shops.filter(item =>
      Object.keys(item).some(k =>
        String(item[k])
          .toLowerCase()
          .includes(value.toLowerCase())
      )
    );
    setDataSearch(filterTable)
  }
  return (
    <div>
      <ToastContainer/>
      <h1>Quản lý cửa hàng</h1>
      <div style={{display: "flex", flexDirection: "row-reverse"}}>
        <div>
          <Input.Search

            placeholder="Search by..."
            enterButton
            onSearch={handleSearch}
          />
        </div>
        <div style={{width: '100%'}}>
          <span className="mt-1">Sắp xếp:</span>
          <Select
            defaultValue="Sắp xếp theo"
            style={{marginLeft: "10px", width: '150px'}}
            onChange={handleChangeOption}
          >
            <Option value="name" style={{width: '100%'}}>Từ A đến Z</Option>
            <Option value="-name" style={{width: '100%'}}>Từ Z đến A</Option>

          </Select>
        </div>
      </div>
      <Table
        loading={loading}
        columns={columns}
        dataSource={dataSearch == null ? shops : dataSearch}
        style={{margin: "20px 0"}}
        pagination={{
          current: page,
          pageSize: pageSize,
          onChange: (page, pageSize) => {
            setPage(page);
            setPageSize(pageSize)
          }
        }}
      />
    </div>
  );
}

export default ShopPage;
