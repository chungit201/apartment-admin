import React, { useState, useEffect } from 'react';
import { TextField, Button, Input, Container } from '@mui/material';
import { useForm } from 'react-hook-form'
import { add } from '../services/ApiServices/productAPI'
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import { getCategory } from "../services/ApiServices/categoryApi";
import {Select} from 'antd';
import {Option} from "antd/es/mentions";

const AddProductPage = () => {
    const [products, setProducts] = useState([]);
    const [categories, setCategories] = useState([])

    const {
        register,
        handleSubmit,
    } = useForm();
    useEffect(() => {
        getDataCategory();
    }, []);

    const getDataCategory = async () => {
        const { data } = await getCategory();
        console.log(data);
        setCategories(data.results);
    }
    const onSubmit = async (product) => {
        try {
            // call api
            const { data } = await add(product);
            const { results } = await data;

            // rerender
            setProducts([...products, results]);
            toast.success("Thêm sản phẩm thành công");
        } catch (error) {

        }
    };
    console.log(products);

    return <Container>
        <ToastContainer />
        <form onSubmit={handleSubmit(onSubmit)}>
            <div style={{ marginBottom: "20px" }}>
                <label>
                    <h1>Thêm sản phẩm mới</h1>
                </label>
            </div>
            <div style={{ marginBottom: "20px" }}>
                <TextField
                    sx={{ width: "100%" }}
                    {...register('name')}
                    required
                    id="outlined-required"
                    label="Tên sản phẩm"
                />
            </div>

            <div style={{ marginBottom: "20px" }}>
                <TextField
                    sx={{ width: "100%" }}
                    {...register('img')}
                    required
                    id="outlined-required"
                    label="Ảnh"
                />
            </div>

            <div style={{ marginBottom: "20px" }}>
                <TextField
                    sx={{ width: "100%" }}
                    {...register('price')}
                    required
                    id="outlined-required"
                    label="Giá"
                />
            </div>

            <div style={{ marginBottom: "20px" }}>
                <TextField
                    sx={{ width: "100%" }}
                    {...register('quantity')}
                    required
                    id="outlined-required"
                    label="Số lượng"

                />
            </div>
            <div>
                <label htmlFor="inputCity" className="form-label">Danh mục</label>
                <select id="product_categoryId" className="form-control" {...register("categories")}>
                    <option> Chọn danh mục</option>
                    {categories.map((cate) => {
                        return (<option  value={cate.id}>{cate.name}</option>)
                    })}
                </select>
            </div>
            <div style={{ marginBottom: "20px" }}>
                <TextField
                    sx={{ width: "100%" }}
                    {...register('description')}
                    required
                    label="Mô tả sản phẩm"
                    multiline
                    rows={4}
                />
            </div>

            <div style={{ marginBottom: "20px" }}>

                <Button type="submit" variant="contained">Thêm</Button>

            </div>
        </form>
    </Container>
};

export default AddProductPage;
