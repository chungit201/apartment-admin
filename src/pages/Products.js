import {useState, useEffect} from 'react';
import {NavLink} from 'react-router-dom'
import {getAll, remove} from '../services/ApiServices/productAPI';
import React from 'react'
// import EditProductPage from './EditProductPage'
import { Table, Space } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'



// components

import {notification} from "antd";


// ----------------------------------------------------------------------




export default function ProductPage() {

    const [products, setProducts] = useState([]);
    useEffect(() => {
        getProducts();
    }, []);
    const getProducts = async () => {
        try {
            const {data} = await getAll();
            const {results} = await data;


            setProducts(results);
            console.log(results);
        } catch (error) {

        }
    };
    const handleRemove = async (id) => {
        try {
            const {data} = await remove(id);
            const newProducts = [...products].filter((item) => item.id !== data.id);
            setProducts(newProducts);
            notification.success({message:data.message})
        } catch (error) {

        }
    };
    const columns = [
        {
            title: 'Tên sản phẩm',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Giá tiền',
            dataIndex: 'price',
            key: 'price',
        },
        {
            title: 'Image',
            dataIndex: 'img',
            key: 'img',
            render: text => <img src={text} width="100px" />,
        },
        {
            title: 'Số lượng',
            dataIndex: 'quantity',
            key: 'quantity',
        },
        {
            title: 'Mô tả',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <NavLink to={`/app/edit-product/${record.id}`}><EditOutlined /></NavLink>
                    <DeleteOutlined onClick={() => handleRemove(record.id)}/>
                </Space>
            ),
        },
    ];


    return (
        <div>
            <h1>Quản lý sản phẩm</h1>
            <div style={{ marginBottom: '20px' }}>

            </div>

            <Table columns={columns} dataSource={products} />
        </div>
    );
}
