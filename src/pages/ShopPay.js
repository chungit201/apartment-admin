import React, {useEffect, useState} from "react";
import {Input, Select, Space, Switch, Table} from "antd";
import moment from "moment"
import {toast, ToastContainer} from "react-toastify";
import {getShop, getShopSortBy} from "../services/ApiServices/shopAPI";
import {updateProduct} from "../services/ApiServices/productAPI";
import {getPaymentShop, updatePaymentShop} from "../services/ApiServices/paymentShop";

const ShopPay = () => {
  const [shops, setShops] = useState([])
  const [loading, setLoading] = useState(true)
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [dataSearch, setDataSearch] = useState(null);
  const [payments, setPayments] = useState([]);


  const {Option} = Select;

  useEffect(() => {
    getDataShop();
    paymentShop();
  }, []);

  const paymentShop = async () => {
    const {data} = await getPaymentShop();
    console.log(data);
    setPayments(data.results);
  }

  const getDataShop = async () => {
    const {data} = await getShop();
    setShops(data.results);
    setLoading(false)
    console.log(data.results)
  }

  const handleChangeOption = async (value) => {
    const {data} = await getShopSortBy(1, 24, value);
    setShops(data.results);
  }

  const handleSearch = (value) => {
    const filterTable = shops.filter(item =>
      Object.keys(item).some(k =>
        String(item[k])
          .toLowerCase()
          .includes(value.toLowerCase())
      )
    );
    setDataSearch(filterTable)
  }

  const columns = [
    {
      title: 'Mã đơn hàng',
      key: 'code',
      render: (record) => (

        <div>{record.code}</div>
      )
    },
    {
      title: 'Tên cửa hàng',
      key: 'shop',
      render: (record) => (
        <div>{record.shop.name}</div>
      )
    },
    {
      title: 'Số tiền',
      key: '',
      render: (record) => (
        <div>{Intl.NumberFormat().format(record.total)} đ</div>
      )
    },
    {
      title: 'Thời gian',
      key: 'time',
      render: (record) => (
        <div>{moment(record.createdAt).format('L')}</div>
        // <div>{record.startDay}</div>
      )
    },
    {
      title: 'Tài khoản ngân hàng',
      key: 'status',
      render: (record) => (
        <div>
          <div>
            Số Tài khoản : {record.shop.bankNumber}
          </div>
          <div>Ngân hàng: {record.shop.bank}</div>
          <div>Ngân hàng: {record.shop.bankName}</div>

        </div>
      )
    },
    {
      title: 'Trạng thái',
      render: (record => {
        return (
          <div>
            <Select defaultValue={record.status} style={{ width: 120 }} onChange={(e)=>handleChange(e,record)}>
              <Option value="pending">Chờ xử lý</Option>
              <Option value="paid">Đã xử lý</Option>
              <Option value="cancelled">Hủy</Option>

            </Select>
          </div>
        )
      })
    }
  ];
  const handleChange = async (value, pyment) => {
    console.log("okok")
    console.log(`selected ${value}`);
    await updatePaymentShop(pyment.id, {
      status: value
    }).then(res => {
      console.log(res)
      if (res.status === 200) {
        toast.success("Cập nhật trạng thái thành công");
      }
    }).catch(err => {
      console.log(err)
    })
  }
  console.log("pppppp",payments)
  return (
    <>
      <div>
        <ToastContainer/>
        <h1>Thanh toán</h1>
        <div style={{display: "flex", flexDirection: "row-reverse"}}>
          <div>
            <Input.Search

              placeholder="Search by..."
              enterButton
              onSearch={handleSearch}
            />
          </div>
          <div style={{width: '100%'}}>
            <span className="mt-1">Sắp xếp:</span>
            <Select
              defaultValue="Sắp xếp theo"
              style={{marginLeft: "10px", width: '150px'}}
              onChange={handleChangeOption}
            >
              <Option value="name" style={{width: '100%'}}>Từ A đến Z</Option>
              <Option value="-name" style={{width: '100%'}}>Từ Z đến A</Option>

            </Select>
          </div>
        </div>
        <Table
          loading={loading}
          columns={columns}
          dataSource={payments}
          style={{margin: "20px 0"}}
          pagination={{
            current: page,
            pageSize: pageSize,
            onChange: (page, pageSize) => {
              setPage(page);
              setPageSize(pageSize)
            }
          }}
        />
      </div>
    </>
  )
}

export default ShopPay