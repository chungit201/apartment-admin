import React, { useState, useEffect } from 'react';
import { TextField, Button, Input, Container, IconButton } from '@mui/material';
import { useForm } from 'react-hook-form'
import { useParams } from 'react-router-dom';
import { readCategory, updateCategory } from "../services/ApiServices/categoryApi";
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";




const EditCategoryPage = () => {
    const { id } = useParams();

    const [categories, setCategories] = useState([])

    const { register, handleSubmit, reset } = useForm();
    useEffect(() => {

        const getCategory = async () => {
            try {

                const { data } = await readCategory(id);
                reset(data);
                console.log(data)
            } catch (error) { }
        };
        getCategory();
    }, [id, reset]);


    const onHandeUpdate = async (category) => {
        try {
            const { data } = await updateCategory(id, category);
            const newCategory = categories.map((item) =>
                item.id === data.id ? data : item
            );
            setCategories(newCategory);
            toast.success(`Cập nhật danh mục thành công`);
        } catch (error) {

        }
    };
    console.log(categories);
    return <Container>
        <ToastContainer />

        <form onSubmit={handleSubmit(onHandeUpdate)}>
            <div style={{ marginBottom: "20px" }}>
                <label>
                    <h1>Cập nhật danh mục</h1>
                </label>
            </div>
            <div style={{ marginBottom: "20px" }}>
                <label>Tên danh mục</label>
                <TextField

                    sx={{ width: "100%" }}
                    {...register('name')}
                    required
                    id="outlined-required"
                />
            </div>

            {/* <div style={{ marginBottom: "20px" }}>
                <label htmlFor="icon-button-file">
                    <Input id="icon-button-file" type="file" />
                    <IconButton color="primary" aria-label="upload picture" component="span">
                        <PhotoCamera />
                    </IconButton>
                </label>
            </div> */}


            <div style={{ marginBottom: "20px" }}>

                <Button type="submit" variant="contained">Cập nhật</Button>

            </div>
        </form>
    </Container>
};

export default EditCategoryPage;
