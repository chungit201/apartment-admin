import React, { useState, useEffect } from 'react';
import { TextField, Button, Input, Container, IconButton } from '@mui/material';
import { useForm } from 'react-hook-form'
import { useParams } from 'react-router-dom';
import { update, read } from '../services/ApiServices/productAPI'
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";




const EditProductPage = ({ product }) => {
    const { id } = useParams();

    const [products, setProducts] = useState([])
    const [productEdit, setProductEdit] = useState();

    const { register, handleSubmit, reset } = useForm();
    useEffect(() => {
        // khai báo hàm getProduct
        const getProduct = async () => {
            try {
                // call API lấy thông tin sản phẩm thông qua ID gửi lên
                const { data } = await read(id);
                reset(data);
                console.log(data);
            } catch (error) { }
        };
        getProduct();
    }, [id, reset]);

    console.log(productEdit);
    const onHandeUpdate = async (product) => {
        try {
            const { data } = await update(id, product);
            const newProducts = products.map((item) =>
                item.id === data.id ? data : item
            );
            setProducts(newProducts);
            toast.success(`Cập nhật sản phẩm thành công`);
        } catch (error) {

        }
    };
    console.log(products);
    return <Container>
        <ToastContainer />

        <form onSubmit={handleSubmit(onHandeUpdate)}>
            <div style={{ marginBottom: "20px" }}>
                <label>
                    <h1>Cập nhật sản phẩm</h1>
                </label>
            </div>
            <div style={{ marginBottom: "20px" }}>
                <label>Tên sản phẩm</label>
                <TextField

                    sx={{ width: "100%" }}
                    {...register('name')}
                    required
                    id="outlined-required"
                />
            </div>

            {/* <div style={{ marginBottom: "20px" }}>
                <label htmlFor="icon-button-file">
                    <Input id="icon-button-file" type="file" />
                    <IconButton color="primary" aria-label="upload picture" component="span">
                        <PhotoCamera />
                    </IconButton>
                </label>
            </div> */}

            <div style={{ marginBottom: "20px" }}>
                <label >Giá</label>
                <TextField
                    sx={{ width: "100%" }}
                    {...register('price')}
                    required
                    id="outlined-required"
                />
            </div>

            <div style={{ marginBottom: "20px" }}>
                <label>Số lượng</label>
                <TextField
                    sx={{ width: "100%" }}
                    {...register('quantity')}
                    required
                    id="outlined-required"

                />
            </div>

            <div style={{ marginBottom: "20px" }}>
                Mô tả
                <TextField
                    sx={{ width: "100%" }}
                    {...register('description')}
                    required
                    multiline
                    rows={4}
                />
            </div>
            <div style={{ marginBottom: "20px" }}>

                <Button type="submit" variant="contained">Cập nhật</Button>

            </div>
        </form>
    </Container>
};

export default EditProductPage;
