import {Table, Tag, Space, notification, Input, Select} from 'antd';
import React, { useEffect, useState } from 'react';
// material
import { getUsers } from "../services/ApiServices/authApi";
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { getCategory, removeCategory, getCategorySortBy } from "../services/ApiServices/categoryApi";
import {NavLink} from "react-router-dom";
import { Button } from '@mui/material';
import {toast} from "react-toastify";
import {getShopSortBy} from "../services/ApiServices/shopAPI";


const CategoriesPage = () => {
  const [categories, setCategories] = useState([])
  const [loading, setLoading] = useState(true)
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(5)
  const [dataSearch, setDataSearch] = useState(null);

  const { Option } = Select;
  useEffect(() => {
    getDataCategory();
  }, []);

  const getDataCategory = async () => {
    const { data } = await getCategory();
    setCategories(data.results);
    setLoading(false)
  }
  const handleRemove = async (id) => {
    try {
      const {data} = await removeCategory(id);
      const newCategory = [...categories].filter((item) => item.id !== data.id);
      setCategories(newCategory);
      toast.success("Xóa danh mục thành công");

    } catch (error) {

    }
  };

  const handleChangeOption = async (value) => {
    const { data } = await getCategorySortBy(1,24,value);
    setCategories(data.results);
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Image',
      dataIndex: 'img',
      key: 'img',
      render: text => <img src={text} width="50px" />,
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">
          <NavLink to={`/app/edit-categories/${record.id}`}><EditOutlined /></NavLink>
          <DeleteOutlined onClick={() => handleRemove(record.id)}/>
        </Space>
      ),
    },
  ];
  const handleSearch = (value) => {
    const filterTable = categories.filter(item =>
        Object.keys(item).some(k =>
            String(item[k])
                .toLowerCase()
                .includes(value.toLowerCase())
        )
    );
    setDataSearch(filterTable)
}
  return (
    <div>
      <h1>Quản lý danh mục</h1>
      <div>
        <NavLink style={{ textDecoration: 'none' }} to="/app/add-category">
          <Button
              variant="contained"
          >
            Thêm danh mục
          </Button>
        </NavLink>
        <div style={{ display: "flex", flexDirection: "row-reverse", marginTop: "20px"}}>
          <div>
            <Input.Search
              style={{ margin: "20px 0"}}
              placeholder="Search by..."
              enterButton
              onSearch={handleSearch}
            />
          </div>
          <div style={{width: '100%'}}>
            <span className="mt-1">Sắp xếp:</span>
            <Select
              defaultValue="Sắp xếp theo"
              style={{marginLeft:"10px",width: '150px'}}
              onChange={handleChangeOption}
            >
              <Option  value="name" style={{width: '100%'}}>Từ A đến Z</Option>
              <Option value="-name" style={{width: '100%'}}>Từ Z đến A</Option>

            </Select>
          </div>
        </div>
      </div>
      <Table 
      loading={loading} 
      columns={columns} 
      dataSource={dataSearch == null ? categories : dataSearch}
      pagination={{
        current: page,
        pageSize:pageSize,
        onChange: (page,pageSize) => {
          setPage(page);
          setPageSize(pageSize)
        }
      }}
      />
    </div>
  );
}

export default CategoriesPage;
