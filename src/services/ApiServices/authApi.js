import instance from "./instance";

export const login = (user) => {
  let url = "/auth/login";
  return instance.post(url, user)
}

export const getUsers = () => {
  let url = "/users";
  return instance.get(url)
}

export const logout = (refreshToken) => {
  let url = "/auth/logout";
  return instance.post(url, refreshToken);
}

export const getUsersSortBy = (page,limit,sortBy) => {
  let url = `/users?page=${page}&limit=${limit}&sortBy=${sortBy}`;
  return instance.get(url)
}