import instance from "./instance";

export const getShop = () => {
    let url = "/shops";
    return instance.get(url)
};
export const removeShop = (id) => {
    const url = "/shops/delete/" + id;
    return instance.post(url);
};
export const readShop = (id) => {
    const url = "/shops/shop-info/" + id;
    return instance.get(url);
};
export const updateShop = ( id,shops) => {
    const url = "/shops/update/" + id;
    return instance.post(url, shops);
};

export const getShopSortBy = (page,limit,sortBy) => {
    let url = `/shops?page=${page}&limit=${limit}&sortBy=${sortBy}`;
    return instance.get(url)
}