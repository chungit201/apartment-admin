import axios from "axios";
const instance = axios.create({
  baseURL:" https://apartment-001.herokuapp.com/api",
  // baseURL:"http://localhost:4000/api"
})

const TOKEN_PAYLOAD_KEY = 'Authorization'

instance.interceptors.request.use(async (config) => {
  const tokens = await (localStorage.getItem('ACCESS_TOKEN'));
  if (tokens) {
    config.headers[TOKEN_PAYLOAD_KEY] = `Bearer ${tokens}`
  }
  return config
})

export default instance;