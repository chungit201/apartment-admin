import instance from "./instance";

export const getPaymentShop = () => {
  const url = "/payment-shop";
  return instance.get(url);
};

export const updatePaymentShop = (paymentId,data) => {
  const url = "/payment-shop/update/" + paymentId;
  return instance.post(url,data);
};

