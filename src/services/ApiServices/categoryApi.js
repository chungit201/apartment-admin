import instance from "./instance";

export const getCategory = () => {
  let url = "/categories?page=1&limit=10000";
  return instance.get(url)
};
export const removeCategory = (id) => {
  const url = "/categories/delete/" + id;
  return instance.post(url);
};
export const addCategory = (category) => {
  const url = "/categories/add";
  return instance.post(url, category);
};
export const readCategory = (id) => {
  const url = "/categories/" + id;
  return instance.get(url);
};
export const updateCategory = ( id,category) => {
  const url = "/categories/edit/" + id;
  return instance.post(url, category);
};

export const getCategorySortBy = (page,limit,sortBy) => {
  let url = `/categories?page=${page}&limit=${limit}&sortBy=${sortBy}`;
  return instance.get(url)
}