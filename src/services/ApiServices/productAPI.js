import instance from "./instance";

export const getAll = () => {
    const url = "/products";
    return instance.get(url);
};

export const remove = (id) => {
    const url = "/products/delete/" + id;
    return instance.post(url);
};
export const add = (product) => {
    const url = "/products/add";
    return instance.post(url, product);
};
export const read = (id) => {
    const url = "/products/" + id;
    return instance.get(url);
};
export const update = ( id,product) => {
    const url = "/products/edit/" + id;
    return instance.patch(url, product);
};
export const lockProduct = ( data) => {
    const url = "/products/lock/all";
    return instance.post(url, data);
};
export const getProductShop = (shopId) =>{
    let url =`/products?shop=${shopId}`;
    return instance.get(url);
}

export const getProductAction = (action,page,limit) =>{
    let url =`/products?action=${action}&page=${page}&limit=${limit}`;
    return instance.get(url);
}

export const updateAllProduct = () => {
    const url = "/products/update/all";
    return instance.post(url);
};

export const updateProduct = ( id,product) => {
    const url = "/products/update/" + id;
    return instance.post(url, product);
};