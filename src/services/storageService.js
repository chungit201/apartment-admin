export const getData = (key, defaultValue) => {
  return new Promise((resolve, reject) => {
    const data = localStorage.getItem(key);
    resolve(data || defaultValue);
  })
}

export const setData = async (key, value) => {
  await localStorage.setItem(key, value);
}

export const removeData = (key) => {
  localStorage.removeItem(key);
}