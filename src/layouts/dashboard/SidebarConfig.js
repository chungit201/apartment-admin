import { Icon } from '@iconify/react';
import pieChart2Fill from '@iconify/icons-eva/pie-chart-2-fill';
import peopleFill from '@iconify/icons-eva/people-fill';
import shoppingBagFill from '@iconify/icons-eva/shopping-bag-fill';
import fileTextFill from '@iconify/icons-eva/file-text-fill';
import lockFill from '@iconify/icons-eva/lock-fill';
import personAddFill from '@iconify/icons-eva/person-add-fill';
import alertTriangleFill from '@iconify/icons-eva/alert-triangle-fill';

// ----------------------------------------------------------------------

const getIcon = (name) => <Icon icon={name} width={22} height={22} />;

const sidebarConfig = [
  {
    title: 'Thống kê',
    path: '/app/',
    icon: getIcon(pieChart2Fill)
  },
  {
    title: 'Phê duyệt',
    path: '/app/approve',
    icon: getIcon(shoppingBagFill)
  },
  {
    title: 'Người dùng',
    path: '/app/user',
    icon: getIcon(peopleFill)
  },
  {
    title: 'Cửa hàng',
    path: '/app/shops',
    icon: <Icon icon="ant-design:shop-outlined" width={22} height={22}/>
  },
  {
    title: 'Thanh toán',
    path: '/app/shop-pay',
    icon: <Icon icon="ant-design:dollar-outlined" width={22} height={22}/>
  },
  {
    title: 'Danh mục',
    path: '/app/categories',
    icon: <Icon icon="prime:book" width={22} height={22}/>
  },

  // {
  //   title: 'login',
  //   path: '/login',
  //   icon: getIcon(lockFill)
  // },
  // {
  //   title: 'register',
  //   path: '/register',
  //   icon: getIcon(personAddFill)
  // },
  // {
  //   title: 'Not found',
  //   path: '/404',
  //   icon: getIcon(alertTriangleFill)
  // }
];

export default sidebarConfig;
