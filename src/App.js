
import ThemeConfig from './theme';
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import DashboardLayout from './layouts/dashboard';
import Login from './pages/Login';
import Register from './pages/Register';
import DashboardApp from './pages/DashboardApp';
import Products from './pages/Products';
import Blog from './pages/Blog';
import User from './pages/User';
import AddProductPage from './pages/AddProductPage';
import EditProductPage from './pages/EditProductPage';
import CategoriesPage from "./pages/CategoriesPage";
import ShopPage from './pages/ShopPage';
import EditCategoryPage from "./pages/EditCategoryPage";
import AddCategoryPage from "./pages/AddCategoryPage";
import DetailShop from "./pages/DetailShop";
import ApproveProduct from "./pages/ Approve";
import ShopPay from "./pages/ShopPay";



export default function App() {
  return (
    <BrowserRouter>
        <ThemeConfig>
            <Routes>
              {/* LayoutWebsite */}
              <Route path="/*" element={<DashboardLayout />}>
                <Route index element={<Navigate to="/app" replace />} />
                <Route path='app' element={<DashboardApp />} />
                <Route path='app/user' element={<User />} />
                <Route path="app/products" element={<Products />} />
                <Route path="app/blog" element={<Blog />} />
                <Route path="app/add-product" element={<AddProductPage />} />
                <Route path="app/edit-product/:id" element={<EditProductPage />} />
                <Route path='app/categories' element={<CategoriesPage />} />
                <Route path="app/add-category" element={<AddCategoryPage />} />
                <Route path='app/categories/:id' element={<DetailShop />} />
                <Route path='app/approve' element={<ApproveProduct />} />
                <Route path="app/edit-categories/:id" element={<EditCategoryPage />} />
                <Route path='app/shops' element={<ShopPage />} />
                <Route path='app/shop-pay' element={<ShopPay />} />
                <Route path='app/shops/:id' element={<DetailShop />} />

              </Route>
              <Route path="register" element={<Register />} />
              <Route path="login" element={<Login />} />
              <Route path="*" element={<Navigate to="/404" replace/>} />
            </Routes>
        </ThemeConfig>
    </BrowserRouter>
  );
}
